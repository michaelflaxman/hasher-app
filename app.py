import os
from flask import Flask, request
from flask.ext.sqlalchemy import SQLAlchemy

from hashlib import sha256

from sqlalchemy.dialects.postgresql import JSON

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class HashedJSON(db.Model):
    __tablename__ = 'hashed_json'

    id = db.Column(db.Integer, primary_key=True)
    # http://stackoverflow.com/questions/12154129/how-can-i-automatically-populate-sqlalchemy-database-fields-flask-sqlalchemy:
    created_on = db.Column(db.DateTime, server_default=db.func.now())
    calculated_hash = db.Column(db.String(), index=True)
    original_json = db.Column(JSON)

    def __init__(self, calculated_hash, original_json):
        self.calculated_hash = calculated_hash
        self.original_json = original_json

    def __repr__(self):
        return '<id {}>'.format(self.id)


@app.route('/')
def hello():
    return "Hello world!"


@app.route('/calculate-hash/', methods=['POST'])
def hash_it():
    # parse as stream to avoid python JSON handling error caused by:
    # {"foo": 1, "bar": 2} != {"bar": 2, "foo": 1}
    post_to_hash = request.stream.read()
    post_hashed = sha256(post_to_hash).hexdigest()

    # Check if already in DB (to prevent duplicate entries) not required
    if not HashedJSON.query.filter_by(calculated_hash=post_hashed).first():
        # Add to DB
        hashed_json_obj = HashedJSON(
                calculated_hash=post_hashed,
                original_json=post_to_hash,
                )
        db.session.add(hashed_json_obj)
        db.session.commit()

    return '%s\n' % post_hashed


@app.route('/get-json/<b64_hash>')
def hello_name(b64_hash):
    hashed_json_obj = HashedJSON.query.filter_by(
            calculated_hash=b64_hash).first()
    if hashed_json_obj:
        return '%s\n' % hashed_json_obj.original_json
    else:
        return 'JSON not found in DB. Did you previously POST it?\n', 404


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
